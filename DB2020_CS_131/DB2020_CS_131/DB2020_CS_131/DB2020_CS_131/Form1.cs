﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DB2020_CS_131
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();


            SqlCommand cmd = new SqlCommand("INSERT  INTO  Student values( @FirstName, @LastName, @Contact, @Email,@RegistrationNumber,@Status)", con);

            cmd.CommandType = CommandType.Text;
    
            cmd.Parameters.AddWithValue("@FirstName", textBox2.Text);
            cmd.Parameters.AddWithValue("@LastName", textBox3.Text);
            cmd.Parameters.AddWithValue("@Contact", textBox4.Text);
            cmd.Parameters.AddWithValue("@Email", textBox5.Text);
            cmd.Parameters.AddWithValue("@RegistrationNumber", textBox6.Text);
            cmd.Parameters.AddWithValue("@Status", textBox7.Text);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Student Added Successfully!!!!!!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("DELETE Student WHERE RegistrationNumber=@reg", con);
            cmd.Parameters.AddWithValue("@reg", textBox6.Text);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Record Deleted Successfully!");
        }

        private void button3_Click(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE Student SET FirstName= @F_Name, LastName=@L_Name, Contact=@Contact, Email=@Email,Status=@Status WHERE RegistrationNumber=@Reg", con);
         
            cmd.Parameters.AddWithValue("@F_Name", textBox2.Text);
            cmd.Parameters.AddWithValue("@L_Name", textBox3.Text);
            cmd.Parameters.AddWithValue("@Contact", textBox4.Text);

            cmd.Parameters.AddWithValue("@Email", textBox5.Text);

            cmd.Parameters.AddWithValue("@Reg", textBox6.Text);
            cmd.Parameters.AddWithValue("@Status", textBox7.Text);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Record Updated Successfully Click Show data to View");

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select FirstName ,LastName,Contact,Email,Status from Student where RegistrationNumber=@reg", con);
            cmd.Parameters.AddWithValue("@reg", textBox6.Text);
            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {
                textBox2.Text = da.GetValue(0).ToString();
                textBox3.Text = da.GetValue(1).ToString();
                textBox4.Text = da.GetValue(2).ToString();
                textBox5.Text = da.GetValue(3).ToString();
                textBox7.Text = da.GetValue(4).ToString();
            }
            if (textBox6.Text == "")
            {
                textBox2.Clear();
                textBox3.Clear();
                textBox4.Clear();
                textBox5.Clear();
                textBox7.Clear();

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student where RegistrationNumber=@reg", con);
            cmd.Parameters.AddWithValue("@reg", textBox6.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();


            SqlCommand cmd = new SqlCommand("INSERT  INTO  Clo values( @Name,@DateCreated,@DateUpdated)", con);

            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@Name", textBox1.Text);
            cmd.Parameters.AddWithValue("@DateCreated", textBox8.Text);
            cmd.Parameters.AddWithValue("@DateUpdated", textBox9.Text);
           
            cmd.ExecuteNonQuery();
            MessageBox.Show("Clo Successfully Added!!!!!!!!!!");
        }

        private void button7_Click(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView2.DataSource = dt;
        }

        private void button8_Click(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete from Clo WHERE Name=@name", con);
            cmd.Parameters.AddWithValue("@name", textBox1.Text);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Clo Record Deleted Successfully!!!!!!!");
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Name ,DateCreated,DateUpdated from Clo where Name=@name", con);
            cmd.Parameters.AddWithValue("@name", textBox1.Text);
            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {
                textBox8.Text = da.GetValue(1).ToString();
                textBox9.Text = da.GetValue(2).ToString();
              
            }
            if (textBox1.Text == "")
            {
                textBox8.Clear();
                textBox9.Clear();

            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo where Name=@name", con);
            cmd.Parameters.AddWithValue("@name", textBox1.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView2.DataSource = dt;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();


            SqlCommand cmd = new SqlCommand("INSERT  INTO  Rubric values(@Id, @Details,@CloId)", con);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Id", textBox11.Text);
            cmd.Parameters.AddWithValue("@Details", textBox10.Text);
            cmd.Parameters.AddWithValue("@CloId", comboBox1.Text);
            

            cmd.ExecuteNonQuery();
            MessageBox.Show("Rubric Successfully Added!!!!!!!!!!");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Clo ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {

                comboBox1.Items.Add(dr["Id"].ToString());


            }
            MessageBox.Show("CLOs IDs Loaded Successfully!!!!!");
        }

        private void button13_Click(object sender, EventArgs e)
        {
             comboBox2.Items.Clear();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Rubric ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {

                comboBox2.Items.Add(dr["Id"].ToString());


            }
            MessageBox.Show("Rubrics IDs Loaded Successfully!!!!!");
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void button14_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView3.DataSource = dt;
        }

        private void button15_Click(object sender, EventArgs e)
        {
        }

        private void button16_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();


            SqlCommand cmd = new SqlCommand("INSERT  INTO  Assessment values(@Title,@DateCreated,@TotalMarks,@TotalWeightage)", con);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Title", textBox14.Text);
            cmd.Parameters.AddWithValue("@DateCreated", textBox15.Text);
            cmd.Parameters.AddWithValue("@TotalMarks", textBox16.Text);
            cmd.Parameters.AddWithValue("@TotalWeightage", textBox17.Text);


            cmd.ExecuteNonQuery();
            MessageBox.Show("Assessment Details Added Successfully!!!!!11");
        }

        private void button17_Click(object sender, EventArgs e)
        {
            comboBox3.Items.Clear();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Rubric ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {

                comboBox3.Items.Add(dr["Id"].ToString());


            }
            MessageBox.Show("Rubrics IDs Loaded Successfully!!!!!");
        }

        private void button18_Click(object sender, EventArgs e)
        {
            comboBox4.Items.Clear();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Assessment ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {

                comboBox4.Items.Add(dr["Id"].ToString());


            }
            MessageBox.Show("Assessment IDs Loaded Successfully!!!!!");
        }

        private void button15_Click_1(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete from Rubric WHERE CloId=@Id", con);
            cmd.Parameters.AddWithValue("@Id", comboBox1.Text);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Clo Record Deleted Successfully!!!!!!!"); 
        }

        private void button19_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();


            SqlCommand cmd = new SqlCommand("INSERT  INTO  RubricLevel values(@RubricId, @Details,@MeasurementLevel)", con);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@RubricId", comboBox2.Text);
            cmd.Parameters.AddWithValue("@Details", textBox12.Text);
            cmd.Parameters.AddWithValue("@MeasurementLevel", textBox13.Text);
           


            cmd.ExecuteNonQuery();
            MessageBox.Show("Rubric Level Successfully Added!!!!!!!!!!");
        }

        private void button9_Click(object sender, EventArgs e)
        {

        }
    }
    }
    

