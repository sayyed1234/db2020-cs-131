﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DB2020__CS__131
{
    public partial class Student : Form
    {
        public Student()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!(textBox1.Text == "") && !(textBox2.Text == "") && !(textBox3.Text == "") && !(textBox4.Text == "") && !(textBox5.Text == "") && !(textBox6.Text == ""))
            {
                var con = Configuration.getInstance().getConnection();


                SqlCommand cmd = new SqlCommand("INSERT  INTO  Student values( @FirstName, @LastName, @Contact, @Email,@RegistrationNumber,@Status)", con);

                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("@FirstName", textBox1.Text);
                cmd.Parameters.AddWithValue("@LastName", textBox2.Text);
                cmd.Parameters.AddWithValue("@Contact", textBox3.Text);
                cmd.Parameters.AddWithValue("@Email", textBox4.Text);
                cmd.Parameters.AddWithValue("@RegistrationNumber", textBox5.Text);
                cmd.Parameters.AddWithValue("@Status", textBox6.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Student Added Successfully!!!!!!");

            }
            else
            {
                MessageBox.Show("Empty Fields Can't be Added");
            }
           
        }

        private void button2_Click(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void Student_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("DELETE Student WHERE RegistrationNumber=@reg", con);
            cmd.Parameters.AddWithValue("@reg", textBox5.Text);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Record Deleted Successfully!");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE Student SET FirstName= @F_Name, LastName=@L_Name, Contact=@Contact, Email=@Email,Status=@Status WHERE RegistrationNumber=@Reg", con);

            cmd.Parameters.AddWithValue("@F_Name", textBox1.Text);
            cmd.Parameters.AddWithValue("@L_Name", textBox2.Text);
            cmd.Parameters.AddWithValue("@Contact", textBox3.Text);

            cmd.Parameters.AddWithValue("@Email", textBox4.Text);

            cmd.Parameters.AddWithValue("@Reg", textBox5.Text);
            cmd.Parameters.AddWithValue("@Status", textBox6.Text);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Record Updated Successfully Click Show data to View");
        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select FirstName ,LastName,Contact,Email,Status from Student where RegistrationNumber=@reg", con);
            cmd.Parameters.AddWithValue("@reg", textBox5 .Text);
            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {
                textBox1.Text = da.GetValue(0).ToString();
                textBox2.Text = da.GetValue(1).ToString();
                textBox3.Text = da.GetValue(2).ToString();
                textBox4.Text = da.GetValue(3).ToString();
                textBox6.Text = da.GetValue(4).ToString();
            }
            if (textBox5.Text == "")
            {
                textBox1.Clear();
                textBox2.Clear();
                textBox3.Clear();
                textBox4.Clear();
                textBox6.Clear();

            }
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            this.Hide();
            f.Show();

        }
    }
}
